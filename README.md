## Synopsis

## Compilation

The compiler must be set to `pdflatex` (with `-shell-escape`).
For a full compilation, that is, including bibliography and glossary, run:

```
pdflatex -synctex=1 -interaction=nonstopmode -shell-escape %.tex|biber %|makeglossaries %|pdflatex -synctex=1 -interaction=nonstopmode -shell-escape %.tex
```

## License cc-by-sa
